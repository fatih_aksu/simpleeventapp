//
//  CachedKeychainManager.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 15.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import KeychainSwift

class CachedKeychainManager {
    static let shared = CachedKeychainManager()
    
    private init() {
        
    }
    
    //caching
    private var keyValueDic: [String: String] = [:]
    
    func set(_ value: String, forKey key: String) {
        let keychain = KeychainSwift()
        
        if let val = keyValueDic[key] {
            if val == value {
                return
            }
        } else {
            if let val = keychain.get(key) {
                if val == value {
                    keyValueDic[key] = val
                    
                    return
                }
            }
        }
        
        keychain.set(value, forKey: key)
        keyValueDic[key] = value
    }
    
    func get(_ key: String) -> String? {
        if let val = keyValueDic[key] {
            return val
        }
        
        let keychain = KeychainSwift()
        let val: String? = keychain.get(key)
        
        if val != nil {
            keyValueDic[key] = val
        }
        
        return val
    }
}
