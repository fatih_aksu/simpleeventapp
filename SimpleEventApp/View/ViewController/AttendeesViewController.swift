//
//  AttendeesTableViewController.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 15.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import UIKit

class AttendeesViewController: UIViewController {
    @IBOutlet var attendeesViewModel: AttendeesViewModel!
    
    var eventId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        attendeesViewModel.listAttendees(eventId: eventId!)
    }
}
