//
//  EventsTableViewController.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 14.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import UIKit

class EventsViewController: UIViewController {
    @IBOutlet var eventsViewModel: EventsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventsViewModel.listEvents()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is AttendeesViewController {
            let vc = segue.destination as? AttendeesViewController
            vc?.eventId = eventsViewModel.eventId
        }
    }
}
