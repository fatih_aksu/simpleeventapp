//
//  NetworkService.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 13.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import Alamofire
import Unbox

class NetworkService {
    
    private enum Constants {
        enum URL {
            case base
            case login
            case events
            case attendees(Int)
            
            var urlString: String {
                switch self {
                case .base:
                    return Constants.URL.baseURLString
                case .login:
                    return Constants.URL.baseURLString + "/apps/api/auth"
                case .events:
                    return Constants.URL.baseURLString + "/apps/api/events"
                case .attendees(let eventId):
                    return Constants.URL.baseURLString + "/apps/api/events/\(eventId)/attendees"
                }
            }
            
            static let baseURLString: String = "https://www.boomset.com"
        }
        
        enum Keys: String {
            case username = "username"
            case password = "password"
            case authorization = "Authorization"
        }
        
        static let contentType: String = "application/json"
        static let token: String = "token"
    }
    
    static func tryLogin(username: String, password: String, completion: @escaping (LoginResponse?) -> Void) {
        let parameters: [String: String] = [
            Constants.Keys.username.rawValue: username,
            Constants.Keys.password.rawValue: password
        ]
        
        Alamofire.request(Constants.URL.login.urlString, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .validate(statusCode: 200..<300)
            .validate(contentType: [Constants.contentType])
            .responseData { response in
                switch response.result {
                case .success:
                    
                    if let data = response.data,
                        let jsonString = String(data: data, encoding: .utf8) {
                        print("Response")
                        print(jsonString)
                    }
                    
                    if let data = response.data {
                        do {
                            let loginResponse: LoginResponse = try unbox(data: data)
                            print(loginResponse.token)
                            
                            CachedKeychainManager.shared.set(loginResponse.token, forKey: Constants.token)
                            
                            completion(loginResponse)
                        } catch {
                            print("An error occurred: \(error)")
                            
                            completion(nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                    
                    completion(nil)
                }
        }
    }
    
    static func listEvents(completion: @escaping (ListEventsResponse?) -> Void) {
        let token = CachedKeychainManager.shared.get(Constants.token)
        
        let headers: HTTPHeaders = [
            Constants.Keys.authorization.rawValue: "Token " + token!
        ]
        
        Alamofire.request(Constants.URL.events.urlString, method: .get, headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: [Constants.contentType])
            .responseData { response in
                switch response.result {
                case .success:
                    
                    if let data = response.data, let jsonString = String(data: data, encoding: .utf8) {
                        print("Response")
                        print(jsonString)
                    }
                    
                    if let data = response.data {
                        do {
                            let listEventsResponse: ListEventsResponse = try unbox(data: data)
                            
                            completion(listEventsResponse)
                        } catch {
                            print("An error occurred: \(error)")
                            
                            completion(nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                    
                    completion(nil)
                }
        }
    }
    
    static func listAttendees(eventId: Int, completion: @escaping (ListAttendeesResponse?) -> Void) {
        let token = CachedKeychainManager.shared.get(Constants.token)
        
        let headers: HTTPHeaders = [
            Constants.Keys.authorization.rawValue: "Token " + token!
        ]
        
        Alamofire.request(Constants.URL.attendees(eventId).urlString, method: .get, headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: [Constants.contentType])
            .responseData { response in
                switch response.result {
                case .success:
                    
                    if let data = response.data, let jsonString = String(data: data, encoding: .utf8) {
                        print("Response")
                        print(jsonString)
                    }
                    
                    if let data = response.data {
                        do {
                            let listAttendeesResponse: ListAttendeesResponse = try unbox(data: data)
                            
                            completion(listAttendeesResponse)
                        } catch {
                            print("An error occurred: \(error)")
                            
                            completion(nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                    
                    completion(nil)
                }
        }
    }
}
