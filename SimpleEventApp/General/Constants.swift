//
//  Constants.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 25.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation

struct Constants {
    enum Segue {
        static let login2events = "login2eventssegueidentifier"
        static let events2attendees = "events2attendeessegueidentifier"
    }
}
