//
//  EventsViewModel.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 14.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import UIKit

class EventsViewModel:NSObject, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var eventsViewController: EventsViewController!
    @IBOutlet weak var tableView: UITableView!

    var events: ListEventsResponse?
    
    let dateformatter = DateFormatter()
    
    var eventId: Int?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventsTableViewCell.reuseID, for: indexPath) as! EventsTableViewCell
        
        configureCell(cell, forRowAtIndexPath: indexPath)
        
        return cell
    }
    
    func configureCell(_ cell: EventsTableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        cell.lblEventName.text = events!.results[indexPath.row].name
        cell.lblStart.text = formattedDate(events!.results[indexPath.row].startsat)
        cell.lblEnd.text = formattedDate(events!.results[indexPath.row].endsat)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let eventId = events?.results[indexPath.row].id {
            self.eventId = eventId
            
            eventsViewController.performSegue(withIdentifier: Constants.Segue.events2attendees, sender: self)
        }
    }
    
    func formattedDate(_ date: Date) -> String {
        dateformatter.dateFormat = "MMM d, yyyy"
        
        return dateformatter.string(from: date)
    }
    
    func listEvents() {
        let sv = UIViewController.displaySpinner(onView: eventsViewController.view)
        
        NetworkService.listEvents(completion: {[unowned self] listEventsResponse in
            UIViewController.removeSpinner(spinner: sv)
            
            self.events = listEventsResponse
            
            self.tableView.reloadData()
        })
    }
}
