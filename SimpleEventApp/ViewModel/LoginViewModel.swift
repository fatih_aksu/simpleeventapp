//
//  LoginViewModel.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 13.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import UIKit

class LoginViewModel:NSObject {
    @IBOutlet weak var loginViewController: LoginViewController!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    func tryLogin() {
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        
        if isValidEmail(testStr: username) == false || password.count < 1 {
            showFailureAlert()
            
            return
        }
        
        let sv = UIViewController.displaySpinner(onView: loginViewController.view)
        
        NetworkService.tryLogin(username: username, password: password, completion: {[unowned self] loginResponse in
            UIViewController.removeSpinner(spinner: sv)
            
            if loginResponse != nil {
                self.loginViewController.performSegue(withIdentifier: Constants.Segue.login2events, sender: nil)
            } else {
                self.showFailureAlert()
            }
        })
    }
    
    func showFailureAlert() {
        let alert = UIAlertController(title: "Username or Password is wrong!", message: nil, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        loginViewController.present(alert, animated: true)
    }
    
    private func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

