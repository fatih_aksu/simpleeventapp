//
//  AttendeesViewModel.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 15.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import UIKit

class AttendeesViewModel:NSObject, UITableViewDataSource {
    @IBOutlet weak var attendeesViewController: AttendeesViewController!
    @IBOutlet weak var tableView: UITableView!
    
    var attendees: ListAttendeesResponse?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attendees?.results.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AttendeesTableViewCell.reuseID, for: indexPath) as! AttendeesTableViewCell
        
        configureCell(cell, forRowAtIndexPath: indexPath)
        
        return cell
    }
    
    func configureCell(_ cell: AttendeesTableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        cell.lblPrefix.text = attendees!.results[indexPath.row].contact.prefix
        cell.lblName.text = attendees!.results[indexPath.row].contact.first_name
        cell.lblSurname.text = attendees!.results[indexPath.row].contact.last_name
        cell.lblCompany.text = attendees!.results[indexPath.row].work_info.company
        cell.lblTitle.text = attendees!.results[indexPath.row].work_info.title
    }
    
    func listAttendees(eventId: Int) {
        let sv = UIViewController.displaySpinner(onView: attendeesViewController.view)
        
        NetworkService.listAttendees(eventId: eventId, completion: { [unowned self] listAttendeesResponse in
            UIViewController.removeSpinner(spinner: sv)
            
            self.attendees = listAttendeesResponse
            
            self.tableView.reloadData()
        })
    }
}
