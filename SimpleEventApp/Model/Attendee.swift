//
//  Attendee.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 15.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import Unbox

struct Attendee {
    let id: Int
    let contact: Contact
    let work_info: WorkInfo
}

extension Attendee: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.contact = try unboxer.unbox(key: "contact")
        self.work_info = try unboxer.unbox(key: "work_info")
    }
}

struct Contact {
    let prefix: String?
    let first_name: String
    let last_name: String
}

extension Contact: Unboxable {
    init(unboxer: Unboxer) throws {
        self.prefix = unboxer.unbox(key: "prefix")
        self.first_name = try unboxer.unbox(key: "first_name")
        self.last_name = try unboxer.unbox(key: "last_name")
    }
}


struct WorkInfo {
    let title: String?
    let company: String?
}

extension WorkInfo: Unboxable {
    init(unboxer: Unboxer) throws {
        self.title = unboxer.unbox(key: "title")
        self.company = unboxer.unbox(key: "company")
    }
}
