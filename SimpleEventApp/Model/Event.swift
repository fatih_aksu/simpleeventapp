//
//  Event.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 15.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import Unbox

struct Event {
    let id: Int
    let group: Group
    let name: String
    let startsat: Date
    let endsat: Date
    let timezone: Int
    let premium: Bool
    let created: Date
    let modified: Date
}

extension Event: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.group = try unboxer.unbox(key: "group")
        self.name = try unboxer.unbox(key: "name")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        self.startsat = try unboxer.unbox(key: "startsat", formatter: dateFormatter)
        self.endsat = try unboxer.unbox(key: "endsat", formatter: dateFormatter)
        self.timezone = try unboxer.unbox(key: "timezone")
        self.premium = try unboxer.unbox(key: "premium")
        self.created = try unboxer.unbox(key: "created", formatter: dateFormatter)
        self.modified = try unboxer.unbox(key: "modified", formatter: dateFormatter)
    }
}

struct Group {
    let id: Int
    let name: String
    let email: String?
    let phone: String?
    let website: String?
    let fbpage: String?
    let twitter: String?
    let googleplus: String?
}

extension Group: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.name = try unboxer.unbox(key: "name")
        self.email = unboxer.unbox(key: "email")
        self.phone = unboxer.unbox(key: "phone")
        self.website = unboxer.unbox(key: "website")
        self.fbpage = unboxer.unbox(key: "fbpage")
        self.twitter = unboxer.unbox(key: "twitter")
        self.googleplus = unboxer.unbox(key: "googleplus")
    }
}
