//
//  ListEventsResponse.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 15.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import Unbox

struct ListEventsResponse {
    let count: Int
    let previous: String?
    let results: [Event]
    let next: String?
}

extension ListEventsResponse: Unboxable {
    init(unboxer: Unboxer) throws {
        self.count = try unboxer.unbox(key: "count")
        self.previous = unboxer.unbox(key: "previous")
        self.results = try unboxer.unbox(key: "results")
        self.next = unboxer.unbox(key: "next")
    }
}
