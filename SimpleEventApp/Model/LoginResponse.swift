//
//  Login.swift
//  SimpleEventApp
//
//  Created by Fatih Aksu on 14.05.2018.
//  Copyright © 2018 Fatih Aksu. All rights reserved.
//

import Foundation
import Unbox

struct LoginResponse {
    let token: String
}

extension LoginResponse: Unboxable {
    init(unboxer: Unboxer) throws {
        self.token = try unboxer.unbox(key: "token")
    }
}
